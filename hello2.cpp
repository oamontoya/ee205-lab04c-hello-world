/////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// @file hello2.cpp
// @version 1.0
// 
// Implements Hello World in C++
//
// @author Osiel Montoya <montoyao@hawaii.edu>
// @date   02_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){

   std::cout << "Hello World";
   std::cout << std::endl;

}
