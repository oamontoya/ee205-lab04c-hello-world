#############################################################################
## University of Hawaii, College of Engineering
## EE 205 - Object Oriented Programming
## Lab 04c - Hello World
##
## @file Makefile
## @version 1.0
## 
## Implements Hello World in C++
##
## @author Osiel Montoya <montoyao@hawaii.edu>
## @date   02_FEB_2021
###############################################################################

all: hello1 hello2

hello1:
	g++ -o hello1 hello1.cpp

hello2:
	g++ -o hello2 hello2.cpp

clean:
	rm -f *.o hello1 hello2
